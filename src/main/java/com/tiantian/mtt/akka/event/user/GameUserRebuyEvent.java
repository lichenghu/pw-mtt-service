package com.tiantian.mtt.akka.event.user;

import com.tiantian.mtt.akka.event.GameUserEvent;

/**
 *
 */
public class GameUserRebuyEvent implements GameUserEvent {
    private String userId;
    private String tableId;
    private String gameId;
    private int buyInCnt;

    public GameUserRebuyEvent(String userId, String gameId, int buyInCnt) {
        this.userId = userId;
        this.gameId = gameId;
        this.buyInCnt = buyInCnt;
    }

    @Override
    public String userId() {
        return userId;
    }

    @Override
    public void setTbId(String tbId) {
        this.tableId = tbId;
    }

    @Override
    public String gameId() {
        return gameId;
    }

    @Override
    public String event() {
        return "userRebuy";
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTableId() {
        return tableId;
    }

    public void setTableId(String tableId) {
        this.tableId = tableId;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public int getBuyInCnt() {
        return buyInCnt;
    }

    public void setBuyInCnt(int buyInCnt) {
        this.buyInCnt = buyInCnt;
    }
}
