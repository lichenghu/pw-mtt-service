package com.tiantian.mtt.handler;

import com.alibaba.fastjson.JSON;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.result.UpdateResult;
import com.tiantian.core.proxy_client.AccountIface;
import com.tiantian.core.thrift.account.UserDetail;
import com.tiantian.mail.proxy_client.MailIface;
import com.tiantian.message.proxy_client.MessageIface;
import com.tiantian.message.thrift.message.MessageInfo;
import com.tiantian.message.thrift.message.MessageType;
import com.tiantian.mtt.akka.ClusterClientManager;
import com.tiantian.mtt.akka.event.game.GameJoinStatusCheckEvent;
import com.tiantian.mtt.akka.event.game.GameStartEvent;
import com.tiantian.mtt.akka.event.user.*;
import com.tiantian.mtt.data.mongodb.MGDatabase;
import com.tiantian.mtt.data.redis.RedisUtil;
import com.tiantian.mtt.model.*;
import com.tiantian.mtt.thrift.*;
import org.apache.commons.lang.StringUtils;
import org.apache.thrift.TException;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.*;

/**
 *
 */
public class MttHandler implements MttService.Iface {
    static Logger LOG = LoggerFactory.getLogger(MttHandler.class);
    private static final String MTT_GAME = "mtt_game";
    private static final String MTT_USER_GAMES_KEY = "mtt_user_games_key:";

    @Override
    public String getServiceVersion() throws TException {
        return mttConstants.version;
    }

    @Override
    public void execMttTask(String gameId) throws TException {
        MttGame mttGame = getMttGameById(gameId);
        LOG.info("StartMttTask,gameId:" + gameId);
        if (mttGame == null) {
            LOG.info("StartMttTask");
            return;
        }
        LOG.info("mttTask:" + JSON.toJSONString(mttGame));
        if (mttGame.getStatus() != 0) { //游戏已经开始或者结束
            return;
        }
        //判断人数是否足够
        int users = mttGame.getMttTableUsers().size();
        if (users < mttGame.getMinUsers()) {
            //解散mtt比赛并归还报名费,并发送邮件通知和消息推送
            disbandMtt(mttGame);
            return;
        }
        // 发送游戏开始信息
        GameStartEvent.Game game = new GameStartEvent.Game();
        game.setGameId(mttGame.getGameId());
        game.setGameName(mttGame.getGameName());
        game.setTaxFee(mttGame.getTaxFee());
        game.setPoolFee(mttGame.getPoolFee());
        game.setTableUsers(mttGame.getTableUsers());
        game.setMinUsers(mttGame.getMaxUsers());
        game.setMaxUsers(mttGame.getMaxUsers());
        game.setPerRestMins(mttGame.getPerRestMins());
        game.setRestMins(mttGame.getRestMins());
        game.setStartBuyIn(mttGame.getStartBuyIn());
        game.setBuyInCnt(mttGame.getBuyInCnt());
        game.setSignDelayMins(mttGame.getSignDelayMins());
        game.setStartMill(mttGame.getStartMills());
        game.setCanRebuyBlindLvl(mttGame.getCanRebuyBlindLvl());
        List<GameStartEvent.Reward> rewards = new ArrayList<>();
        if (mttGame.getRewards() != null) {
            for (MttReward mttReward : mttGame.getRewards()) {
                 GameStartEvent.Reward reward = new GameStartEvent.Reward();
                 reward.setRanking(mttReward.getRanking());
                 reward.setVirtualType(mttReward.getVirtualType());
                 reward.setVirtualNums(mttReward.getVirtualNums());
                 reward.setPhysicalId(mttReward.getPhysicalId());
                 reward.setPhysicalName(mttReward.getPhysicalName());
                 rewards.add(reward);
            }
        }
        game.setRewards(rewards);
        List<GameStartEvent.Rule> rules = new ArrayList<>();
        if (mttGame.getRules() != null) {
            for (MttRule mttRule : mttGame.getRules()) {
                 GameStartEvent.Rule rule = new GameStartEvent.Rule();
                 rule.setLevel(mttRule.getLevel());
                 rule.setSmallBlind(mttRule.getSmallBlind());
                 rule.setBigBlind(mttRule.getBigBlind());
                 rule.setAnte(mttRule.getAnte());
                 rule.setUpgradeSecs(mttRule.getUpgradeSecs());
                 rule.setStoreSecs(mttRule.getStoreSecs());
                 rule.setReBuyIn(mttRule.getReBuyIn());
                 rule.setCostMoney(mttRule.getCostMoney());
                 rules.add(rule);
            }
        }
        game.setRules(rules);
        List<GameStartEvent.UserInfo> userInfos = new ArrayList<>();
        Set<String> userIds = new HashSet<>();
        for (MttTableUser mttTableUser : mttGame.getMttTableUsers()) {
             String userId = mttTableUser.getUserId();
             userIds.add(userId);
             GameStartEvent.UserInfo userInfo = new GameStartEvent.UserInfo();
             userInfo.setUserId(userId);
             try {
                 UserDetail userDetail = AccountIface.instance().iface().findUserDetailByUserId(userId);
                 if (userDetail != null) {
                     userInfo.setNickName(userDetail.getNickName());
                     userInfo.setAvatarUrl(userDetail.getAvatarUrl());
                     userInfo.setGender(userDetail.getGender());
                 }
             }
             catch (Exception e) {
             }
             userInfos.add(userInfo);
        }
        GameStartEvent gameStartEvent = new GameStartEvent(mttGame.getGameId(), game, new ArrayList<>(userIds), userInfos);
        boolean ret = (boolean) ClusterClientManager.sendAndWait(gameStartEvent, 30);
        // 更新游戏状态开始游戏
        if (ret) {
            //TODO 初始化添加的用户
            // 删除redis中的报名数据
            deleteMttSignInfo(mttGame);
            updateGameStart(gameId);
            // 通知玩家已经开始
            noticeMttStart(new ArrayList<>(userIds), gameId, mttGame.getGameName());
        } else {
            LOG.error("start mtt fail; table_id:" + mttGame.getGameId());
        }
    }

    @Override
    public MttJoinOrCancelGameResult signMttGame(String userId, String gameId) throws TException {
        MttGame mttGame = getMttGameById(gameId);
        if (mttGame == null) {
            return new MttJoinOrCancelGameResult("-1", "比赛不存在", "", 0, false);
        }
        if(mttGame.getStatus() < 0) {
            return new MttJoinOrCancelGameResult("-2", "比赛已经结束", "", 0, false);
        }
        if (System.currentTimeMillis() > mttGame.getStartMills() + mttGame.getSignDelayMins() * 60000) {
            return new MttJoinOrCancelGameResult("-3", "比赛报名已截止", "", 0, false);
        }
        List<String> userIds = getMttAllUsers(mttGame);
        if (userIds != null && userIds.size() > mttGame.getMaxUsers()) {
            return new MttJoinOrCancelGameResult("-4", "比赛报名人数已满", "", 0, false);
        }
        // 判断是否已经报名了
        if (checkUserHasSignMtt(userId, mttGame.getMttTableUsers())) {
             return new MttJoinOrCancelGameResult("0", "报名成功", mttGame.getGameId(),
                     mttGame.getMttTableUsers().size(), false);
        }
        //扣除报名费是否足够
        long fee = mttGame.getTaxFee() + mttGame.getPoolFee();
        boolean ret = AccountIface.instance().iface().reduceUserMoney(userId, fee);
        if (!ret) {
            return new MttJoinOrCancelGameResult("-5", "报名费不足", "", 0, false);
        }

        //  判断游戏是否已经是延迟报名
        boolean isDelay =  mttGame.getStatus() == 1 &&
                (System.currentTimeMillis() < mttGame.getStartMills() + mttGame.getSignDelayMins() * 60000);
        //添加到记录中
        UserDetail userDetail = AccountIface.instance().iface().findUserDetailByUserId(userId);
        String nickName = null;
        String avatarUrl = null;
        String gender = null;
        long chips = mttGame.getStartBuyIn();
        if (userDetail != null) {
            nickName = userDetail.getNickName();
            avatarUrl = userDetail.getAvatarUrl();
            gender = userDetail.getGender();
        }
        userSign(userId, nickName, avatarUrl, chips, gameId);
        // 判断游戏是否已经是延迟报名 然后推送消息
        if (isDelay) {
            // 发送命令到游戏服务器 玩家处理
            ClusterClientManager.send(new GameUserDelaySignEvent(gameId, userId, nickName, avatarUrl, gender));
        }
        return new MttJoinOrCancelGameResult("0", "报名成功", mttGame.getGameId(), mttGame.getMttTableUsers().size() + 1,
                isDelay);
    }

    @Override
    public MttResponse enterMttGame(String userId, String gameId) throws TException {
        //TODO 退出其他的mtt比赛
        MttGame mttGame = getMttGameById(gameId);
        if (mttGame == null || mttGame.getStatus() < 0) {
            return new MttResponse("-1", "比赛已经结束");
        }
        if (mttGame.getStatus() == 0) {
            return new MttResponse("-2", "比赛未开始");
        }
        boolean hasJoin = checkUserHasSignMtt(userId, mttGame.getMttTableUsers());
        if (!hasJoin) {
            return new MttResponse("-3", "玩家比赛结束");
        }
        boolean ret = (boolean) ClusterClientManager.sendAndWait(new GameUserEnterEvent(gameId, userId));
        if (ret ) {
            return new MttResponse("0", "进入游戏成功");
        } else {
            return new MttResponse("-4", "进入游戏失败");
        }
    }

    @Override
    public MttJoinOrCancelGameResult cancelMttGame(String userId, String gameId) throws TException {
        MttGame mttGame = getMttGameById(gameId);
        if (mttGame != null && mttGame.getStatus() == 0) { // 游戏必须是未开始状态
            boolean canCancel = false;
            for (MttTableUser mttTableUser : mttGame.getMttTableUsers()) {
                if (mttTableUser.getUserId().equalsIgnoreCase(userId) &&
                        "waiting".equalsIgnoreCase(mttTableUser.getStatus())) {
                    canCancel = true;
                    break;
                }
            }
            if (canCancel) {
                // 从mongo中删除
                MongoCollection<Document> collection = MGDatabase.getInstance().getDB().getCollection(MTT_GAME);

                BasicDBObject updateCondition = new BasicDBObject();
                updateCondition.put("_id", new ObjectId(gameId));

                // 删除数据
                BasicDBObject updatedValue = new BasicDBObject();
                updatedValue.put("mttTableUsers", new BasicDBObject("userId", userId));
                BasicDBObject updateSetValue = new BasicDBObject("$pull", updatedValue);

                UpdateResult result = collection.updateOne(updateCondition, updateSetValue);
                boolean ret = result.getModifiedCount() > 0;
                if (!ret) {
                    return new MttJoinOrCancelGameResult("-1","取消失败", "", 0, false);
                }
                long money = mttGame.getTaxFee() + mttGame.getPoolFee();
                try {
                    AccountIface.instance().iface().addUserMoney(userId, money);
                } catch (TException e) {
                    LOG.error("return user money fail; userId :" + userId+ ",money :" + money);
                    e.printStackTrace();
                }
                return new MttJoinOrCancelGameResult("0", "报名成功", mttGame.getGameId(),
                        Math.max(0, mttGame.getMttTableUsers().size() - 1), false);
            }
        }
        return new MttJoinOrCancelGameResult("-2","取消失败,游戏已开始", "", 0, false);
    }

    @Override
    public List<MttGameInfo> mttGameInfos(String userId, String type, long startMills, int nums) throws TException {
        //TODO 暂时未分页
        MongoCollection<Document> collection = MGDatabase.getInstance().getDB().getCollection(MTT_GAME);
        BasicDBObject selectCondition = new BasicDBObject();
        BasicDBList values = new BasicDBList();
        values.add(0);
        values.add(1);
        selectCondition.put("status", new BasicDBObject("$in",  values));
        if (StringUtils.isNotBlank(type) && !"all".equalsIgnoreCase(type)) { // 不是所有比赛
            selectCondition.put("type", type);
        }
        selectCondition.put("available", 1);
        FindIterable<Document> limitIter = collection.find(selectCondition)
                .sort(new BasicDBObject("startMills", 1));
        MongoCursor<Document> cursor = limitIter.iterator();
        List<MttGameInfo> list = new ArrayList<>();
        while (cursor.hasNext()) {
               Document doc = cursor.next();
               MttGame mttGame = getFromMttGameDoc(doc);
               MttGameInfo mttGameInfo = new MttGameInfo();
               mttGameInfo.setGameId(mttGame.getGameId());
               mttGameInfo.setGameName(mttGame.getGameName());
               mttGameInfo.setMinUsers(mttGame.getMinUsers());
               mttGameInfo.setMaxUsers(mttGame.getMaxUsers());
               mttGameInfo.setRewardMark(mttGame.getRewardMark());
               mttGameInfo.setStartDate(mttGame.getStartMills());
               mttGameInfo.setPoolFee(mttGame.getPoolFee());
               mttGameInfo.setTaxFee(mttGame.getTaxFee());
               mttGameInfo.setJoinUsers(mttGame.getMttTableUsers().size());
               mttGameInfo.setRebuyMark("第"+mttGame.getCanRebuyBlindLvl()+"级别可以rebuy"+mttGame.getBuyInCnt()+"次数");
               mttGameInfo.setFeetype("金币");
               boolean hasSign = checkUserHasSignMtt(userId, mttGame.getMttTableUsers());
               // 判断玩家是否加入了
               if (mttGame.getStatus() == 0) { //未开始
                   if (hasSign) {
                       mttGameInfo.setStatus("cancel"); //cancel (取消),  delay (延时报名), join (进入游戏), sign (报名)
                   } else {
                       mttGameInfo.setStatus("sign");
                   }
               } else { // 游戏已经开始
                   if (hasSign) {
                       mttGameInfo.setStatus("join");
                   }
                   else {
                       //判断是否延迟
                       if (System.currentTimeMillis() < mttGame.getStartMills() + mttGame.getSignDelayMins() * 60000) {
                           mttGameInfo.setStatus("delay");
                       }
                   }
               }
               mttGameInfo.setType(mttGame.getType());
               mttGameInfo.setCanRebuy(mttGame.getCanRebuyBlindLvl() > 0);
               if (mttGame.getRewards() != null && mttGame.getRewards().size() > 0) {
                   mttGameInfo.setRewardUsers(mttGame.getRewards().size());
               }
               if (StringUtils.isNotBlank(mttGameInfo.getStatus())) {
                   list.add(mttGameInfo);
               }
        }
        return list;
    }

    @Override
    public List<MttGameInfo> myMttGameInfos(String userId) throws TException {
        MongoCollection<Document> collection = MGDatabase.getInstance().getDB().getCollection(MTT_GAME);
        BasicDBObject selectCondition = new BasicDBObject();
        BasicDBList values = new BasicDBList();
        values.add(0);
        values.add(1);
        selectCondition.put("status", new BasicDBObject("$in",  values));
        selectCondition.put("available", 1);
        selectCondition.put("mttTableUsers",  new BasicDBObject("$elemMatch",
                         new BasicDBObject().append("userId", userId)
                         .append("status", new BasicDBObject("$ne", "end"))));
        FindIterable<Document> limitIter = collection.find(selectCondition)
                .sort(new BasicDBObject("startMills", 1));
        MongoCursor<Document> cursor = limitIter.iterator();
        List<MttGameInfo> list = new ArrayList<>();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            MttGame mttGame = getFromMttGameDoc(doc);
            MttGameInfo mttGameInfo = new MttGameInfo();
            mttGameInfo.setGameId(mttGame.getGameId());
            mttGameInfo.setGameName(mttGame.getGameName());
            mttGameInfo.setMinUsers(mttGame.getMinUsers());
            mttGameInfo.setMaxUsers(mttGame.getMaxUsers());
            mttGameInfo.setRewardMark(mttGame.getRewardMark());
            mttGameInfo.setStartDate(mttGame.getStartMills());
            mttGameInfo.setPoolFee(mttGame.getPoolFee());
            mttGameInfo.setTaxFee(mttGame.getTaxFee());
            mttGameInfo.setJoinUsers(mttGame.getMttTableUsers().size());
            mttGameInfo.setFeetype("金币");
            // 判断玩家是否加入了
            if (mttGame.getStatus() == 0) { //未开始
                mttGameInfo.setStatus("cancel"); //cancel (取消),  delay (延时报名), join (进入游戏), sign (报名)
            } else { // 游戏已经开始
               mttGameInfo.setStatus("join");
            }
            mttGameInfo.setType(mttGame.getType());
            if (mttGame.getRewards() != null && mttGame.getRewards().size() > 0) {
                mttGameInfo.setRewardUsers(mttGame.getRewards().size());
            }
            if (StringUtils.isNotBlank(mttGameInfo.getStatus())) {
                list.add(mttGameInfo);
            }
        }
        return list;
    }

    @Override
    public void gameEvent(String cmd, String userId, String gameId, String tableId, String data) throws TException {
        switch (cmd) {
            case "fold":
                GameUserFoldEvent userFoldEvent = new GameUserFoldEvent(userId, data, gameId);
                ClusterClientManager.send(userFoldEvent);
                break;
            case "check":
                GameUserCheckEvent userCheckEvent = new GameUserCheckEvent(userId, data, gameId);
                ClusterClientManager.send(userCheckEvent);
                break;
            case "call":
                GameUserCallEvent userCallEvent = new GameUserCallEvent(userId, data, gameId);
                ClusterClientManager.send(userCallEvent);
                break;
            case "raise":
                try {
                    String[] datas = data.split(",");
                    long raise = Long.parseLong(datas[0]);
                    String pwd = datas[1];
                    GameUserRaiseEvent userRaiseEvent = new GameUserRaiseEvent(userId, raise, pwd, gameId);
                    ClusterClientManager.send(userRaiseEvent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case "allin":
                GameUserAllinEvent userAllinEvent = new GameUserAllinEvent(userId, data, gameId);
                ClusterClientManager.send(userAllinEvent);
                break;
            case "fast_raise":
                try {
                    String[] datas = data.split(",");
                    GameUserFastRaiseEvent userFastRaiseEvent = new  GameUserFastRaiseEvent(userId, datas[0], datas[1], gameId);
                    ClusterClientManager.send(userFastRaiseEvent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case "show_cards":
                GameUserShowCardsEvent userShowCardsEvent = new GameUserShowCardsEvent(gameId, userId);
                ClusterClientManager.send(userShowCardsEvent);
                break;
            case "mtt_table_info" :
                GameUserTableInfoEvent tableInfoEvent = new GameUserTableInfoEvent(gameId, userId);
                ClusterClientManager.send(tableInfoEvent);
                break;
        }
    }

    @Override
    public MttGameMark mttGameMark(String userId, String gameId) throws TException {
        MttGame mttGame = getMttGameById(gameId);
        if (mttGame == null) {
            return new MttGameMark();
        }
        MttGameMark mttGameMark = new MttGameMark();
        mttGameMark.setGameId(mttGame.getGameId());
        mttGameMark.setName(mttGame.getGameName());
        mttGameMark.setFee(mttGame.getTaxFee() + mttGame.getPoolFee());
        mttGameMark.setStartTime(mttGame.getStartMills());
        mttGameMark.setMinUsers(mttGame.getMinUsers());
        mttGameMark.setMaxUsers(mttGame.getMaxUsers());
        mttGameMark.setBeginBuyIn(mttGame.getStartBuyIn());
        mttGameMark.setRebuyDesc(mttGame.getRebuyDesc());
        mttGameMark.setFeeType("金币");
        //  判断游戏是否已经是延迟报名
        boolean isDelay =  mttGame.getStatus() == 1 &&
                (System.currentTimeMillis() < mttGame.getStartMills() + mttGame.getSignDelayMins() * 60000);
        String userStatus = getUserGameStatus(userId, mttGame.getMttTableUsers());
        String status = null;
        if ("end".equalsIgnoreCase(userStatus)) {
            status = "";
        } else  if ((isDelay || mttGame.getStatus() == 0) && StringUtils.isBlank(userStatus)) { //玩家未报名,游戏可以进入
                status = "sign";
        } else if (mttGame.getStatus() == 0 && "waiting".equalsIgnoreCase(userStatus)) { // 游戏未开始,玩家是等待状态
            status = "cancel";
        } else if ( mttGame.getStatus() == 1 && !"gaming".equalsIgnoreCase(userStatus)) {
            status = "join";
        }
        mttGameMark.setStatus(status);
        return mttGameMark;
    }

    @Override
    public List<MttGameRule> mttGameRules(String gameId) throws TException {
        MttGame mttGame = getMttGameById(gameId);
        if (mttGame == null) {
            return new ArrayList<>();
        }
        List<MttGameRule> mttGameRules = new ArrayList<>();
        int currentLvl = mttGame.getCurretBlindLvl();
        int i = 0;
        for (MttRule mttRule : mttGame.getRules()) {
             i ++;
             MttGameRule mttGameRule = new MttGameRule();
             mttGameRule.setAnte(mttRule.getAnte());
             mttGameRule.setLvl(i);
             mttGameRule.setIsCurrent(currentLvl == i);
             mttGameRule.setBigBlind(mttRule.getBigBlind());
             mttGameRule.setSmallBlind(mttRule.getSmallBlind());
             mttGameRule.setUpgradeSecs(mttRule.getUpgradeSecs());
             mttGameRules.add(mttGameRule);
        }
        return mttGameRules;
    }

    @Override
    public List<MttGameReward> mttGameRewards(String gameId) throws TException {
        MttGame mttGame = getMttGameById(gameId);
        if (mttGame == null) {
            return new ArrayList<>();
        }
        List<MttReward> mttRewards = mttGame.getRewards();
        List<MttGameReward> mttGameRewards = new ArrayList<>();
        for (MttReward mttReward : mttRewards) {
             MttGameReward mttGameReward = new MttGameReward();
             mttGameReward.setLvl(mttReward.getRanking());
             mttGameReward.setVituralNum(mttReward.getVirtualNums());
             mttGameReward.setVirtualType(mttReward.getVirtualNums());
             mttGameReward.setPhysicalName(mttReward.getPhysicalName());
             mttGameRewards.add(mttGameReward);
        }
        return mttGameRewards;
    }

    @Override
    public List<MttGameRanking> mttGameRankings(String gameId) throws TException {
        MttGame mttGame = getMttGameById(gameId);
        if (mttGame == null) {
            return new ArrayList<>();
        }
        List<MttGameRanking> mttGameRankings = new ArrayList<>();
        List<MttTableUser> mttTableUsers = mttGame.getMttTableUsers();
        if (mttTableUsers  != null && mttTableUsers.size() > 0) {
            Collections.sort(mttTableUsers, (o1, o2) -> {
                if (o1.getLeftChips() != 0 && o2.getLeftChips() != 0) {
                    return (int) (o2.getLeftChips() - o1.getLeftChips());
                }
                if (o1.getLeftChips() == 0 && o2.getLeftChips() == 0) {
                    return o1.getRanking() - o2.getRanking();
                }
                if (o1.getLeftChips() != 0 && o2.getLeftChips() == 0) {
                    return -1;
                }
                if (o1.getLeftChips() == 0 && o2.getLeftChips() != 0) {
                    return 1;
                }
                return 0;
            });
            int i = 0;
            long lastChips = 0;
            for (MttTableUser mttTableUser : mttTableUsers) {
                 if (lastChips == 0 || lastChips != mttTableUser.getLeftChips()) {
                     i++;
                 }
                 lastChips = mttTableUser.getLeftChips();
                 MttGameRanking mttGameRanking = new MttGameRanking();
                 mttGameRanking.setLvl(i);
                 mttGameRanking.setNickName(mttTableUser.getName());
                 mttGameRanking.setChips(mttTableUser.getLeftChips());
                 mttGameRanking.setUserId(mttTableUser.getUserId());
                 mttGameRankings.add(mttGameRanking);
            }
            Collections.sort(mttGameRankings, (o1, o2) -> o1.getLvl() - o2.getLvl());
        }
        return mttGameRankings;
    }

    @Override
    public MttGameDetail mttGameDetail(String gameId) throws TException {
        MttGame mttGame = getMttGameById(gameId);

        MttGameDetail mttGameDetail = new MttGameDetail();
        if (mttGame == null) {
            return mttGameDetail;
        }
        if (mttGame.getStatus() != 1) {
            return mttGameDetail;
        }
        mttGameDetail.setLeftUsers(getNotEndUsers(mttGame.getMttTableUsers()));
        mttGameDetail.setRebuyCnt(getTotalRebuyCnt(mttGame.getMttTableUsers()));
        mttGameDetail.setJoinUsers(mttGame.getMttTableUsers().size());
        mttGameDetail.setRewardUsers(mttGame.getRewards().size());
        mttGameDetail.setAverageChips(getAverageChips(mttGame.getMttTableUsers()));
        mttGameDetail.setGameUsedSecs(Math.max(0, (int) (System.currentTimeMillis() - mttGame.getStartDate()) / 1000));
        mttGameDetail.setCurrentLvl(mttGame.getCurretBlindLvl());
        mttGameDetail.setTotalLvl(mttGame.getRules().size());
        List<MttRule> mttRuleList = mttGame.getRules();
        if (mttGame.getCurretBlindLvl() > 0) {
            int index = mttGame.getCurretBlindLvl() - 1;
            MttRule mttRule = mttRuleList.get(index);
            long nextUpBlindTime = mttGame.getUpdateBlindTimes();
            int leftSecs = (int) (nextUpBlindTime - System.currentTimeMillis()) / 1000;
            mttGameDetail.setUpgradeLeftSecs(Math.max(leftSecs, 0));
            mttGameDetail.setCurrentAnte(mttRule.getAnte());
            mttGameDetail.setCurrentSmallBlind(mttRule.getSmallBlind());
            mttGameDetail.setCurrentBigBlind(mttRule.getBigBlind());
            if (mttGame.getCurretBlindLvl() < mttRuleList.size()) {
                MttRule nextMttRule = mttRuleList.get(index + 1);
                mttGameDetail.setNextAnte(nextMttRule.getAnte());
                mttGameDetail.setNextSmallBlind(nextMttRule.getSmallBlind());
                mttGameDetail.setNextBigBlind(nextMttRule.getBigBlind());
            }
        }
        return mttGameDetail;
    }

    @Override
    public MttResponse exitGame(String gameId, String userId) throws TException {
        MttGame mttGame = getMttGameById(gameId);
        if (mttGame == null) {
            return new MttResponse("0", "退出成功");
        }
        if (mttGame.getStatus() < 0) {
            return new MttResponse("0", "退出成功");
        }
        boolean ret = (boolean) ClusterClientManager.sendAndWait(new GameUserExitEvent(gameId, userId));
        if (ret) {
            return new MttResponse("0", "退出成功");
        }
        return new MttResponse("-1", "退出失败");
    }

    @Override
    public MttResponse userHasJoinGame(String userId, String gameId) throws TException {
        String value = RedisUtil.getFromMap(MTT_USER_GAMES_KEY + userId, gameId);
        if (StringUtils.isNotBlank(value) && "1".equalsIgnoreCase(value)) {
            return new MttResponse("0", "");
        }
        return new MttResponse("-1", "不在局中");
    }

    @Override
    public boolean manualStartMttGame(String gameId) throws TException {
        execMttTask(gameId);
        return true;
    }

    @Override
    public MttResponse userRebuy(String userId, String gameId) throws TException {
        MttGame mttGame = getMttGameById(gameId);
        if (mttGame == null) {
            return new MttResponse("-1", "比赛已结束");
        }
        if (mttGame.getStatus() != 1) {
            return new MttResponse("-2", "比赛已结束");
        }
        if (mttGame.getCanRebuyBlindLvl() <= 0 || mttGame.getBuyInCnt() <= 0) {
            return new MttResponse("-3", "该比赛不能买入");
        }
        int ret = (int) ClusterClientManager.sendAndWait(new GameUserRebuyEvent(userId, gameId, 1));
        LOG.info("User Rebuy :" + ret);
        if (ret == 0) {
            return new MttResponse("0", "买入成功");
        } else {
            if (ret == -10) {
                return new MttResponse("-4", "金币不足");
            }
        }
        return new MttResponse("-5", "买入失败");
    }

    @Override
    public RebuyInfo getRebuyInfo(String userId, String gameId) throws TException {
        //TODO
        return null;
    }

    @Override
    public MttResponse checkUserInMttGame(String userId, String gameId) throws TException {
        boolean isInGame =  (boolean) ClusterClientManager.sendAndWait(new GameJoinStatusCheckEvent(gameId, userId));
        if (isInGame) {
            return new MttResponse("0", "在游戏中");
        }
        return new MttResponse("-1", "桌子已经解散");
    }

    @Override
    public RankingResult getUserMttRanking(String gameId, String userId) throws TException {
        long nums = 0;
        int type = 1;
        String name = "";
        int ranking = 0;
        String mttName = "";
        MttGame mttGame = getMttGameById(gameId);
        if (mttGame != null) {
            mttName = mttGame.getGameName();
            for (MttTableUser mttTableUser : mttGame.getMttTableUsers()) {
                if (userId != null && userId.equalsIgnoreCase(mttTableUser.getUserId())
                        && "end".equalsIgnoreCase(mttTableUser.getStatus())) {
                    ranking = mttTableUser.getRanking();
                    if (mttGame.getRewards() != null) {
                        for (MttReward mttReward : mttGame.getRewards()) {
                            if (ranking == mttReward.getRanking()) {
                                nums = mttReward.getVirtualNums();
                                type = mttReward.getVirtualType();
                                name = mttReward.getPhysicalName();
                                break;
                            }
                        }
                    }
                    break;
                }
            }
        }
        return new RankingResult(ranking, mttName, name, type, nums, System.currentTimeMillis());
    }

    @Override
    public MttResponse returnMttGame(String gameId, String userId) throws TException {
        boolean ret = (boolean)ClusterClientManager.sendAndWait(new GameUserReturnSitEvent(gameId, userId));
        if(ret) {
            return new MttResponse("0", "回到游戏成功");
        }
        return new MttResponse("-1", "回到游戏失败");
    }

    private int getNotEndUsers(List<MttTableUser> list) {
        if (list == null || list.isEmpty()) {
            return 0;
        }
        int i = 0;
        for (MttTableUser mttTableUser : list) {
             if (!"end".equalsIgnoreCase(mttTableUser.getStatus())) {
                 i++;
             }
        }
        return i;
    }
    private int getTotalRebuyCnt(List<MttTableUser> list) {
        if (list == null || list.isEmpty()) {
            return 0;
        }
        int i = 0;
        for (MttTableUser mttTableUser : list) {
             i += mttTableUser.getRebuyCnt();
        }
        return i;
    }

    private long getAverageChips(List<MttTableUser> list) {
        if (list == null || list.isEmpty()) {
            return 0;
        }
        long i = 0;
        long totalChips = 0;
        for (MttTableUser mttTableUser : list) {
            if (!"end".equalsIgnoreCase(mttTableUser.getStatus())) {
                i++;
            }
            totalChips += mttTableUser.getLeftChips();
        }
        if (i == 0) {
            return 0;
        }
        return totalChips / i;
    }

    private List<String> getMttAllUsers(MttGame mttGame) {
        List<String> userIds = new ArrayList<>();
        for (MttTableUser mttTableUser : mttGame.getMttTableUsers()) {
             userIds.add(mttTableUser.getUserId());
        }
        return userIds;
    }

    public boolean checkUserHasSignMtt(String userId, List<MttTableUser> mttTableUsers) {
        if (mttTableUsers != null) {
            for (MttTableUser mttTableUser : mttTableUsers) {
                 if (userId.equalsIgnoreCase(mttTableUser.getUserId()) &&
                            !"end".equalsIgnoreCase( mttTableUser.getStatus())) { // 必须是玩家在游戏中未结束的
                     return true;
                 }
            }
        }
        return false;
    }

    private String getUserGameStatus(String userId, List<MttTableUser> mttTableUsers) {
        if (mttTableUsers != null) {
            for (MttTableUser mttTableUser : mttTableUsers) {
                if (userId.equalsIgnoreCase(mttTableUser.getUserId())) { // 必须是玩家在游戏中未结束的
                    return mttTableUser.getStatus();
                }
            }
        }
        return "";
    }

    private boolean userSign(String userId, String nickName, String avatarUrl, long chips, String gameId) {
        MongoCollection<Document> collection = MGDatabase.getInstance().getDB().getCollection(MTT_GAME);
        BasicDBObject updateCondition = new BasicDBObject();
        updateCondition.put("_id", new ObjectId(gameId));
        BasicDBObject listItem = new BasicDBObject("mttTableUsers",
                new BasicDBObject("userId", userId)
                        .append("nickName", nickName)
                        .append("avatarUrl", avatarUrl)
                        .append("joinTime", System.currentTimeMillis())
                        .append("leftChips", chips)
                        .append("ranking", 0)
                        .append("rebuyCnt", 0)
                        .append("status", "waiting"));
        BasicDBObject updateSetValue = new BasicDBObject("$push", listItem); // 注意并发问题会重复
        UpdateResult result = collection.updateOne(updateCondition, updateSetValue);
        return (result.getModifiedCount() > 0);
    }

    private void deleteMttSignInfo(MttGame mttGame) {
        //TODO 未实现
    }

    private void disbandMtt(MttGame mttGame) {
        List<String> allUserIds = getMttAllUsers(mttGame);
        if (allUserIds != null) {
            long money = mttGame.getTaxFee() + mttGame.getPoolFee();
            for (String userId : allUserIds) {
                 try {
                     AccountIface.instance().iface().addUserMoney(userId, money);
                 } catch (TException e) {
                    LOG.error("return user money fail; userId :" + userId+ ",money :" + money);
                    e.printStackTrace();
                 }
            }
            String mail = "你报名的" + mttGame.getGameName() + "因报名人数不足解散,报名费已经自动返还";
            for (String userId : allUserIds) {
                 try {
                     MailIface.instance().iface().saveNotice(userId, mail);
                 } catch (TException e) {
                     e.printStackTrace();
                 }
                 MessageInfo messageInfo = new MessageInfo();
                 messageInfo.setToUserId(userId);
                 messageInfo.setMessageEvent("mtt_disband");
                 messageInfo.setMessageType(MessageType.SHORT_MESSAGE);
                 messageInfo.setMessageContent(mail);
                 try {
                     MessageIface.instance().iface().send(messageInfo);
                 } catch (TException e) {
                     e.printStackTrace();
                 }
            }
        }
        updateGameDisband(mttGame.getGameId());
    }

    private void noticeMttStart(List<String> userIds, String gameId, String gameName) {
        for (String userId : userIds) {
            MessageInfo messageInfo = new MessageInfo();
            messageInfo.setToUserId(userId);
            messageInfo.setMessageEvent("mtt_start");
            messageInfo.setMessageType(MessageType.SHORT_MESSAGE);
            messageInfo.setMessageContent("你报名的" + gameName + "比赛已经开始.|" + gameId);
            try {
                MessageIface.instance().iface().send(messageInfo);
            } catch (TException e) {
                e.printStackTrace();
            }
        }
    }

    private MttGame getMttGameById(String gameId) {
        MongoCollection<Document> collection = MGDatabase.getInstance().getDB().getCollection(MTT_GAME);
        BasicDBObject selectCondition = new BasicDBObject();
        selectCondition.put("_id", new ObjectId(gameId));
        FindIterable<Document> limitIter = collection.find(selectCondition);
        MongoCursor<Document> cursor = limitIter.iterator();
        while (cursor.hasNext()) {
               Document doc = cursor.next();
               return getFromMttGameDoc(doc);
        }
        return null;
    }

    public boolean updateGameStart(String gameId) {
        return updateGameStatus(gameId, 1);
    }

    private boolean updateGameDisband(String gameId) {
        return updateGameStatus(gameId, -2);
    }

    private boolean updateGameStatus(String gameId, int status) {
        MongoCollection<Document> collection = MGDatabase.getInstance().getDB().getCollection(MTT_GAME);
        BasicDBObject updateCondition = new BasicDBObject();
        updateCondition.put("_id", new ObjectId(gameId));

        BasicDBObject updatedValue = new BasicDBObject();
        updatedValue.put("status", status);
        updatedValue.put("startDate", System.currentTimeMillis());
        updatedValue.put("curretBlindLvl", 1);
        BasicDBObject updateSetValue = new BasicDBObject("$set", updatedValue);
        UpdateResult result = collection.updateOne(updateCondition, updateSetValue);
        return (result.getModifiedCount() > 0);
    }

    private MttGame getFromMttGameDoc(Document doc) {
        MttGame mttGame = new MttGame();
        ObjectId objectId = doc.getObjectId("_id");
        mttGame.setGameId(objectId.toString());
        String gameName = doc.getString("gameName");
        mttGame.setGameName(gameName);
        Long taxFee = doc.getLong("taxFee");
        mttGame.setTaxFee(taxFee);
        Long poolFee = doc.getLong("poolFee");
        mttGame.setPoolFee(poolFee);
        Integer tableUsers = doc.getInteger("tableUsers");
        mttGame.setTableUsers(tableUsers);
        Integer minUsers = doc.getInteger("minUsers");
        mttGame.setMinUsers(minUsers);
        Integer maxUsers = doc.getInteger("maxUsers");
        mttGame.setMaxUsers(maxUsers);
        Long startMills = doc.getLong("startMills");
        mttGame.setStartMills(startMills);
        Integer status = doc.getInteger("status");
        mttGame.setStatus(status);
        Integer curretBlindLvl = doc.getInteger("curretBlindLvl");
        mttGame.setCurretBlindLvl(curretBlindLvl);
        Long updateBlindTimes = doc.getLong("updateBlindTimes");
        mttGame.setUpdateBlindTimes(updateBlindTimes);
        Integer perRestMins = doc.getInteger("perRestMins");
        mttGame.setPerRestMins(perRestMins);
        Integer restMins = doc.getInteger("restMins");
        mttGame.setRestMins(restMins);
        Integer startBuyIn = doc.getInteger("startBuyIn");
        mttGame.setStartBuyIn(startBuyIn);
        Integer buyInCnt = doc.getInteger("buyInCnt");
        mttGame.setBuyInCnt(buyInCnt);
        String rebuyDesc = doc.getString("rebuyDesc");
        mttGame.setRebuyDesc(rebuyDesc);
        Integer signDelayMins = doc.getInteger("signDelayMins");
        mttGame.setSignDelayMins(signDelayMins);
        Long createDate = doc.getLong("createDate");
        mttGame.setCreateDate(createDate);
        Long startDate = doc.getLong("startDate");
        mttGame.setStartDate(startDate);
        Long endDate = doc.getLong("endDate");
        mttGame.setEndDate(endDate);
        String rewardMark = doc.getString("rewardMark");
        mttGame.setRewardMark(rewardMark);
        String type = doc.getString("type");
        mttGame.setType(type);
        int available = doc.getInteger("available");
        mttGame.setAvailable(available);
        Integer canRebuyBlindLvl = doc.getInteger("canRebuyBlindLvl");
        mttGame.setCanRebuyBlindLvl(canRebuyBlindLvl);
        List<Document> rewards = doc.get("rewards", List.class);
        List<MttReward> mttRewards = new ArrayList<>();
        if (rewards != null && rewards.size() > 0) {
            for(Document rewardDoc : rewards) {
                MttReward mttReward = new MttReward();
                Integer ranking = rewardDoc.getInteger("ranking");
                mttReward.setRanking(ranking);
                Integer virtualType = rewardDoc.getInteger("virtualType");
                mttReward.setVirtualType(virtualType);
                Long virtualNums = rewardDoc.getLong("virtualNums");
                mttReward.setVirtualNums(virtualNums);
                String physicalId = rewardDoc.getString("physicalId");
                mttReward.setPhysicalId(physicalId);
                String physicalName = rewardDoc.getString("physicalName");
                mttReward.setPhysicalName(physicalName);
                mttRewards.add(mttReward);
            }
        }
        mttGame.setRewards(mttRewards);
        List<Document> rules = doc.get("rules", List.class);
        List<MttRule> mttRuleList = new ArrayList<>();
        if (rules != null && rules.size() > 0) {
            for (Document ruleDoc : rules) {
                 MttRule mttRule = new MttRule();
                 Integer level = ruleDoc.getInteger("level");
                 Long smallBlind = ruleDoc.getLong("smallBlind");
                 Long bigBlind = ruleDoc.getLong("bigBlind");
                 Long ante = ruleDoc.getLong("ante");
                 Integer upgradeSecs = ruleDoc.getInteger("upgradeSecs");
                 Integer storeSecs = ruleDoc.getInteger("storeSecs");
                 Integer reBuyIn = ruleDoc.getInteger("reBuyIn");
                 Long costMoney = ruleDoc.getLong("costMoney");
                 if (costMoney == null) {
                     costMoney = 0L;
                 }
                 mttRule.setLevel(level);
                 mttRule.setSmallBlind(smallBlind);
                 mttRule.setBigBlind(bigBlind);
                 mttRule.setAnte(ante);
                 mttRule.setUpgradeSecs(upgradeSecs);
                 mttRule.setStoreSecs(storeSecs);
                 mttRule.setReBuyIn(reBuyIn);
                 mttRule.setCostMoney(costMoney);
                 mttRuleList.add(mttRule);
            }
        }
        mttGame.setRules(mttRuleList);
        List<Document> tableUsersDoc = doc.get("mttTableUsers", List.class);
        List<MttTableUser> mttTableUsers = new ArrayList<>();
        if (tableUsersDoc != null && tableUsersDoc.size() > 0) {
            for (Document tableUserDoc : tableUsersDoc) {
                 MttTableUser mttTableUser = new MttTableUser();
                 mttTableUser.setUserId(tableUserDoc.getString("userId"));
                 mttTableUser.setName(tableUserDoc.getString("nickName"));
                 mttTableUser.setAvatarUrl(tableUserDoc.getString("avatarUrl"));
                 mttTableUser.setJoinTime(tableUserDoc.getLong("joinTime"));
                 mttTableUser.setRanking(tableUserDoc.getInteger("ranking"));
                 mttTableUser.setLeftChips(tableUserDoc.getLong("leftChips"));
                 mttTableUser.setStatus(tableUserDoc.getString("status"));
                 mttTableUsers.add(mttTableUser);
            }
        }
        mttGame.setMttTableUsers(mttTableUsers);
        return mttGame;
    }
}
