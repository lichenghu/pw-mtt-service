package com.tiantian.mtt.proxy_client;

import com.tiantian.framework.thrift.client.ClientPool;
import com.tiantian.framework.thrift.client.IFace;
import com.tiantian.mtt.settings.MttConfig;
import com.tiantian.mtt.thrift.MttService;
import org.apache.thrift.TServiceClient;
import org.apache.thrift.protocol.TProtocol;

/**
 *
 */
public class MttIface extends IFace<MttService.Iface> {
    private static class MttIfaceHolder {
        private final static MttIface instance = new MttIface();
    }

    private MttIface() {
    }
    public static MttIface instance() {
        return MttIfaceHolder.instance;
    }

    @Override
    protected TServiceClient createClient(TProtocol tProtocol) {
        return new MttService.Client(tProtocol);
    }

    @Override
    protected ClientPool createPool() {
        return new ClientPool(MttConfig.getInstance().getHost(), MttConfig.getInstance().getPort());
    }

    @Override
    protected Class<MttService.Iface> faceClass() {
        return MttService.Iface.class;
    }
}
