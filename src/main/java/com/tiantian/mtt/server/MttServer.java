package com.tiantian.mtt.server;

import com.tiantian.mtt.akka.ClusterClientManager;
import com.tiantian.mtt.data.mongodb.MGDatabase;
import com.tiantian.mtt.data.postgresql.PGDatabase;
import com.tiantian.mtt.handler.MttHandler;
import com.tiantian.mtt.settings.MttConfig;
import com.tiantian.mtt.settings.PGConfig;
import com.tiantian.mtt.thrift.MttService;
import com.tiantian.mtt.timerTask.TimePollingManager;
import org.apache.thrift.TProcessor;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.server.TThreadPoolServer;
import org.apache.thrift.transport.TFastFramedTransport;
import org.apache.thrift.transport.TServerSocket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class MttServer {
    private static Logger LOG = LoggerFactory.getLogger(MttServer.class);
    private TThreadPoolServer server;
    private TThreadPoolServer.Args sArgs;

    public static void main(String[] args) {
        ClusterClientManager.init((MttConfig.getInstance().getPort() + 5) + "");
        new Thread(TimePollingManager::start).start();
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                TimePollingManager.stop();
            }
        });
        MttServer mttServer = new MttServer();
        mttServer.init(args);
        mttServer.start();
    }

    public void init(String[] arguments) {
        LOG.info("init");
        try {
            PGDatabase.getInstance().init(
                    PGConfig.getInstance().getHost(),
                    PGConfig.getInstance().getPort(),
                    PGConfig.getInstance().getDb(),
                    PGConfig.getInstance().getUsername(),
                    PGConfig.getInstance().getPassword()
            );
            MGDatabase.getInstance().init();

            MttHandler handler = new MttHandler();

            TProcessor tProcessor = new MttService.Processor<MttService.Iface>(handler);
            TServerSocket tss = new TServerSocket(MttConfig.getInstance().getPort());
            sArgs = new TThreadPoolServer.Args(tss);
            sArgs.processor(tProcessor);
            sArgs.transportFactory(new TFastFramedTransport.Factory());
            sArgs.protocolFactory(new TCompactProtocol.Factory());

        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void start() {
        LOG.info("start");
        server = new TThreadPoolServer(sArgs);
        LOG.info("the server listen in {}", MttConfig.getInstance().getPort());
        server.serve();
    }

    public void stop() {
        LOG.info("stop");
        server.stop();
    }

    public void destroy() {
        LOG.info("destroy");
    }
}
