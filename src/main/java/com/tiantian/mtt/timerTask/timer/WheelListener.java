package com.tiantian.mtt.timerTask.timer;

import java.util.List;

/**
 *
 */
public interface WheelListener<E> {
    void wheelElements(List<E> elments);
}
