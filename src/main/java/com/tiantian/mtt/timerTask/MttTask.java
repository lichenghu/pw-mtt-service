package com.tiantian.mtt.timerTask;

import java.io.Serializable;

/**
 * mtt游戏开始任务
 */
public class MttTask implements Serializable{
    private String gameId;
    private long startTime;

    public MttTask(String gameId, long startTime) {
        this.gameId = gameId;
        this.startTime = startTime;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }
}
