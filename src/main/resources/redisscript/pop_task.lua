local keyLen = #KEYS
if keyLen ~= 2 then
    return {}
end
local currentTime = KEYS[1]
local taskKey = KEYS[2]
local result = {}
local i = 0;
while true
do
    i = i + 1
    if i > 1000 then
        redis.log(redis.LOG_NOTICE, "error")
        return result
    end
    local memers = redis.pcall("ZRANGE", taskKey,  0, 0,  "WITHSCORES")
    if not memers then
        return result
    end
    local gameId = memers[1]
    local startTime = memers[2]
    if not gameId then
        return result
    end

    if tonumber(startTime) - currentTime < 3*60*1000 then
       -- 需要放入队列中返回
       table.insert(result, gameId)
       table.insert(result, startTime .. "")
       -- 删除头
       redis.pcall("ZREMRANGEBYRANK", taskKey,  0, 0)
    else
        return result
    end
end




